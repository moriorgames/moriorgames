<?php

namespace Morior\Bundle\EnchantToolsBundle\Entity;

/**
 * TileMap
 */
class TileMap
{

// --------------------------------- CONSTANTS ---------------------------------
    
    const BOX_BACKGROUND        = 0;
    const BOX_FOREGROUND        = 1;
    const BOX_OBJECT_GROUND     = 2;
    const BOX_OBJECT_COLISION   = 3;
    const BOX_WALL              = 4;

// --------------------------------- PROPERTIES --------------------------------

    /**
     * @var integer
     *
     * Identifier
     */
    protected $id;

    /**
     * @var Map
     */
    protected $map;

    /**
     * @var integer
     */
    protected $box;
    
    /**
     * @var integer
     */
    protected $boxType;

// ---------------------------------- GETTERS ----------------------------------

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \Morior\Bundle\EnchantToolsBundle\Map $map
     */
    public function getMap()
    {
        return $this->map;
    }

    /**
     * @return integer
     */
    public function getBox()
    {
        return $this->box;
    }

    /**
     * @return integer
     */
    public function getBoxType()
    {
        return $this->boxType;
    }

// ---------------------------------- SETTERS ----------------------------------

    /**
     * @param \Morior\Bundle\EnchantToolsBundle\Map $map
     * @return \Morior\Bundle\EnchantToolsBundle\TileMap
     */
    public function setMap(Map $map)
    {
        $this->map = $map;

        return $this;
    }

    /**
     * @param integer $box
     * @return \Morior\Bundle\EnchantToolsBundle\TileMap
     */
    public function setBox($box)
    {
        $this->box = $box;

        return $this;
    }

    /**
     * @param integer $boxType
     * @return \Morior\Bundle\EnchantToolsBundle\TileMap
     */
    public function setBoxType($boxType)
    {
        $this->boxType = $boxType;
        
        return $this;
    }

}
