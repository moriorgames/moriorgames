<?php

namespace Morior\Bundle\EnchantToolsBundle\Entity;

/**
 * Map
 */
class Map
{

// --------------------------------- PROPERTIES --------------------------------

    /**
     * @var integer
     *
     * Identifier
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var integer
     */
    protected $height;

    /**
     * @var integer
     */
    protected $width;

// ---------------------------------- GETTERS ----------------------------------

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return integer
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @return integer
     */
    public function getWidth()
    {
        return $this->width;
    }

// ---------------------------------- SETTERS ----------------------------------

    /**
     * @param string $name
     * @return $this Self object
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param integer $height
     * @return $this Self object
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @param integer $width
     * @return $this Self object
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

}
