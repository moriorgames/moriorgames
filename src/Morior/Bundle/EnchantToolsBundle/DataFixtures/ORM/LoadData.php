<?php

namespace Morior\Bundle\EnchantToolsBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use Morior\Bundle\EnchantToolsBundle\Entity\Map;

/**
 * Class LoadData
 */
class LoadData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $map = new Map();
        $map
            ->setName('DefaultMap')
            ->setHeight(5)
            ->setWidth(5);

        $manager->persist($map);
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 10;
    }

}
