<?php

namespace Morior\Bundle\EnchantToolsBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MapEditorControllerTest extends WebTestCase
{
    /**
     *
     */
    public function testEnchantToolsMapEmpty()
    {
        $client = static::createClient();

        // Get the empty map create url
        $crawler = $client->request('GET', 'enchant-tools/map-create');

        $this->assertIdPage($crawler, $client);

        // Get the form and assert the name is empty
        $value = $crawler
            ->filter('form[name=enchant_map]')
            ->form()
            ->get('enchant_map[name]')
            ->getValue();

        $this->assertEquals('', $value);
    }

    /**
     *
     */
    public function testEnchantToolsMapId()
    {
        $client = static::createClient();

        // Get the map id:1 create url
        $crawler = $client->request('GET', 'enchant-tools/map-create/1');

        $this->assertIdPage($crawler, $client);

        // Get the form and assert the name is the fixtures 1
        $mapName = $this->getIdMap1Name();

        $this->assertEquals('DefaultMap', $mapName);
    }

    /**
     *
     */
    public function testEnchantToolsMapPOST()
    {
        $client = static::createClient();

        // Get the map id:1 create url
        $crawler = $client->request('GET', 'enchant-tools/map-create/1');

        $this->assertIdPage($crawler, $client);

        // Get the form and change the name, then submit
        $newName = 'newMap';
        $form = $crawler->filter('form[name=enchant_map]')->form();
        $form->get('enchant_map[name]')->setValue($newName);
        $client->submit($form);

        // Get the form and assert the name is the fixtures 1
        $mapName = $this->getIdMap1Name();

        $this->assertEquals($newName, $mapName);
    }

// --------------------------------- PRIVATE --------------------------------

    /**
     * @param $crawler
     * @param $client
     */
    private function assertIdPage($crawler, $client)
    {
        // The page is reachable
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        // We found the id of the div page
        $link = $crawler->filter('#_enchant_tools_create');
        $this->assertTrue(count($link) > 0);
    }

    /**
     * @return mixed
     */
    private function getIdMap1Name()
    {
        $client = static::createClient();

        // Get the empty map create url
        $crawler = $client->request('GET', 'enchant-tools/map-create/1');

        // Get the form and assert the name is the fixtures 1
        $name = $crawler
            ->filter('form[name=enchant_map]')
            ->form()
            ->get('enchant_map[name]')
            ->getValue();

        return $name;
    }
}
