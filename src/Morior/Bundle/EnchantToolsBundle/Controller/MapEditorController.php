<?php

namespace Morior\Bundle\EnchantToolsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Morior\Bundle\EnchantToolsBundle\Entity\Map;
use Morior\Bundle\EnchantToolsBundle\Form\Type\MapType;

/**
 * DefaultController
 */
class MapEditorController extends Controller
{
    /**
     * Show the view to create and edit a Map Entity
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        // If there is a map query
        if ($request->get('mapId')) {

            $map = $em
                ->getRepository('MoriorEnchantToolsBundle:Map')
                ->find($request->get('mapId'));

            // If not exist redirect to map empty url
            if (!$map instanceof Map) {
                $url = $this->generateUrl('morior_enchant_tools_map_create');
                return $this->redirect($url);
            }

        } else {

            $map = $this
                ->get('morior.enchant_tools.map_factory')
                ->create();

        }

        $form = $this->createForm(new MapType(), $map);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em->persist($map);
            $em->flush();

            $url = $this->generateUrl('morior_enchant_tools_map_create', [
                'mapId' => $map->getId()
            ]);
            return $this->redirect($url);
        }

        return $this->render('@MoriorEnchantTools/Editor/create.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * @param $mapId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editorAction($mapId)
    {
        return $this->render('@MoriorEnchantTools/Editor/map.html.twig',
            ['mapId' => $mapId]
        );
    }
}
