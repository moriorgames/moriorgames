<?php

namespace Morior\Bundle\EnchantToolsBundle\Factory;

use Morior\Bundle\EnchantToolsBundle\Entity\Map;

/**
 * Class MapFactory
 */
class MapFactory
{

// --------------------------------- METHODS --------------------------------

    /**
     * Creates an instance of a Map.
     *
     * @return Map entity
     */
    public function create()
    {
        $map = new Map();
        $map
            ->setName('')
            ->setHeight(0)
            ->setWidth(0);

        return $map;
    }
}
