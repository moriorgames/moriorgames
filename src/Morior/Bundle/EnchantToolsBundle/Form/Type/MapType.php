<?php

namespace Morior\Bundle\EnchantToolsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class MapType
 */
class MapType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('height')
            ->add('width')
            ->add('save', 'submit');
    }

    public function getName()
    {
        return 'enchant_map';
    }
}
