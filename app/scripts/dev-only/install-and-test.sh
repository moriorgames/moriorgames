#!/bin/bash

app/console doctrine:schema:drop --force

app/console doctrine:schema:update --force

app/console doctrine:fixtures:load --append --em=default \
    --fixtures=src/Morior/Bundle/EnchantToolsBundle/DataFixtures/ORM

phpunit -c app
