<!DOCTYPE html>
<html>
<head>
    <title>Tests</title>
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Language" content="es">
    <meta name="ROBOTS" content="all">

    <link rel="stylesheet" href="css/app.css">

</head>
<body>
<section id="todoapp">
    <header id="header">
        <h1>Todo</h1>
        <input id="new-todo" placeholder="What needs to be done?" autofocus>
    </header>
    <section id="main">
        <ul id="todo-list"></ul>
    </section>
    <footer id="footer">
        <span id="todo-count"></span>
        <ul id="filters">
            <li>
                <a href="#/" class="selected">All</a>
            </li>
            <li>
                <a href="#/active">Active</a>
            </li>
        </ul>
        <button id="clear-completed">Clear completed</button>
    </footer>
</section>
</footer>
<script src="js/config.js"></script>
<script src="js/helpers.js"></script>
<script src="js/store.js"></script>
<script src="js/model.js"></script>
<script src="js/template.js"></script>
<script src="js/view.js"></script>
<script src="js/controller.js"></script>
<script src="js/app.js"></script>

<!--<script type="text/javascript" src="http://static.thronewar.com/cache/js/jquery-1.10.2.min.js"></script>

<script>
    $(document).ready(function(){
        alert('MOlla');
    });
</script>-->
</body>
</html>
